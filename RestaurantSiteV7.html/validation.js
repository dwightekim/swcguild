			function validateForm() {
				var name; var contact1; var contact2; var days; var selectMe; var fillMe;
			
				name = document.forms["myForm"]["name"].value
				contact1 = document.forms["myForm"]["email"].value
				contact2 = document.forms["myForm"]["phone"].value
				days =  document.myForm.day1.checked || document.myForm.day2.checked || document.myForm.day3.checked || document.myForm.day4.checked || document.myForm.day5.checked 
				selectMe = document.getElementById("selectMe").selected
				fillMe = document.getElementById("additionalInformation").value
				
					if (name == null || name=="") {
						alert("Please enter your name.");
						return false;
					}
					if ((contact1 == null || contact1 == "") && 
						(contact2 == null || contact2 == "")) {
						alert("Please provide your email or phone number.");
						return false;
					}
					if ((selectMe == true) && 
						(fillMe == null || fillMe == "")) {
						alert("Please provide additional information regarding your selection.")
						return false;
					}
					if (!days) {	
						alert("Please select at least one day to contact you.");
						return false;
					}
					else {
						return true;
					}
					}